# structurizr


## Getting started with Structurizr

[Structurizr](https://structurizr.com/) builds upon "diagrams as code", allowing you to create multiple software architecture diagrams from a single model. There are a number of tools for creating Structurizr compatible workspaces, with the Structurizr DSL being the recommended option for most teams. This Structurizr DSL template creates a simple start.

To create a template project run
```bash
make structurizr
```

To generate mermaid version run
```bash
make mermaid
```


