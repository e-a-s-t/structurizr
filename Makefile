default: structurizr

.PHONY: all structurizr

structurizr:
	docker run -it --rm -p 8081:8080 -v ${PWD}/structurizr:/usr/local/structurizr structurizr/lite

mermaid:
	docker run -it --rm -v ${PWD}:/usr/local/structurizr structurizr/cli export -workspace /usr/local/structurizr/structurizr/workspace.json -format mermaid -o /usr/local/structurizr/docs/diagrams
